package com.github.axet.catalogsreader.dialogs;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.webkit.WebView;

import com.github.axet.androidlibrary.net.HttpClient;
import com.github.axet.androidlibrary.widgets.ErrorDialog;
import com.github.axet.androidlibrary.widgets.WebViewCustom;
import com.github.axet.catalogsreader.activities.MainActivity;
import com.github.axet.catalogsreader.app.CatalogsApplication;
import com.github.axet.catalogsreader.fragments.NavigatorFragment;
import com.github.axet.catalogsreader.navigators.Search;
import com.github.axet.catalogsreader.net.HttpProxyClient;

public class BrowserDialogFragment extends com.github.axet.androidlibrary.widgets.BrowserDialogFragment {
    public static String TAG = BrowserDialogFragment.class.getSimpleName();

    public static BrowserDialogFragment create(String head, String url, String cookies, String js, String js_post) {
        BrowserDialogFragment f = new BrowserDialogFragment();
        Bundle args = new Bundle();
        args.putString("head", head);
        args.putString("url", url);
        args.putString("js", js);
        args.putString("js_post", js_post);
        args.putString("cookies", cookies);
        f.setArguments(args);
        return f;
    }

    public static BrowserDialogFragment createHtml(String html_base, String head, String html, String js, String js_post) {
        BrowserDialogFragment f = new BrowserDialogFragment();
        Bundle args = new Bundle();
        args.putString("base", html_base);
        args.putString("head", head);
        args.putString("html", html);
        args.putString("js", js);
        args.putString("js_post", js_post);
        f.setArguments(args);
        return f;
    }

    public static BrowserDialogFragment create(String url) {
        BrowserDialogFragment f = new BrowserDialogFragment();
        Bundle args = new Bundle();
        args.putString("url", url);
        f.setArguments(args);
        return f;
    }

    public static BrowserDialogFragment createHtml(String base, String html) {
        BrowserDialogFragment f = new BrowserDialogFragment();
        Bundle args = new Bundle();
        args.putString("base", base);
        args.putString("html", html);
        f.setArguments(args);
        return f;
    }

    public BrowserDialogFragment() {
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        if (web != null) {
            web.destroy();
            web = null;
        }

        final Activity activity = getActivity();
        if (activity instanceof DialogInterface.OnDismissListener)
            ((DialogInterface.OnDismissListener) activity).onDismiss(result);
    }

    MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.startsWith(CatalogsApplication.SCHEME_MAGNET)) {
            if (Search.magnetName(url) == null)
                url = Search.magnetName(url, view.getTitle());
            MainActivity.openTorrent(getContext(), Uri.parse(url));
            return true;
        }
        return false;
    }

    @Override
    public boolean onDownloadStart(final String base, final String url, String userAgent, String contentDisposition, final String mimetype, long contentLength) {
        Log.d(TAG, "onDownloadStart " + url);
        final MainActivity main = getMainActivity();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (main.isFinishing())
                    return;
                MainActivity.openBook(getContext(), Uri.parse(url));
            }
        });
        return true;
    }

    @Override
    public HttpClient createHttpClient() {
        MainActivity main = (MainActivity) getActivity();
        HttpProxyClient http = new HttpProxyClient(main.getCurrentFragment());
        http.update(getContext());
        return http;
    }

    @Override
    public void onErrorMessage(String msg) {
        ErrorDialog.Post(getActivity(), msg);
    }
}
