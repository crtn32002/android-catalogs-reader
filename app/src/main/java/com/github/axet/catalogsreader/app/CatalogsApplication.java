package com.github.axet.catalogsreader.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.github.axet.androidlibrary.app.MainApplication;
import com.github.axet.androidlibrary.app.NotificationManagerCompat;
import com.github.axet.androidlibrary.app.Storage;
import com.github.axet.androidlibrary.net.HttpClient;
import com.github.axet.androidlibrary.widgets.NotificationChannelCompat;
import com.github.axet.bookreader.app.BookApplication;
import com.github.axet.catalogsreader.R;

import org.geometerplus.zlibrary.core.util.SystemInfo;
import org.geometerplus.zlibrary.ui.android.library.ZLAndroidApplication;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

public class CatalogsApplication extends MainApplication {
    final String TAG = CatalogsApplication.class.getSimpleName();

    public static String SCHEME_MAGNET = "magnet";
    public static String SCHEME_TORRENT = "application/x-bittorrent";

    public static final String DOTSLASH = "./";

    public static final String PREFERENCE_STORAGE = "storage_path";
    public static final String PREFERENCE_SCREENLOCK = "screen_lock";
    public static final String PREFERENCE_THEME = "theme";
    public static final String PREFERENCE_ANNOUNCE = "announces_list";
    public static final String PREFERENCE_LAST_PATH = "lastpath";
    public static final String PREFERENCE_DIALOG = "dialog";
    public static final String PREFERENCE_RUN = "run";
    public static final String PREFERENCE_PROXY = "proxy";
    public static final String PREFERENCE_PROXY_PREFIX = "proxy_";
    public static final String PREFERENCE_OPTIMIZATION = "optimization";
    public static final String PREFERENCE_NEXT = "next";
    public static final String PREFERENCE_PLAYER = "player";
    public static final String PREFERENCE_BOOT = "boot";
    public static String PREFERENCE_CATALOGS = "catalogs";
    public static String PREFERENCE_CATALOGS_PREFIX = "catalogs_";
    public static String PREFERENCE_CATALOGS_COUNT = "count";
    public static String PREFERENCE_LIBRARY_LAYOUT = "layout_";
    public static String PREFERENCE_SORT = "sort";

    public ZLAndroidApplication zlib;

    public static CatalogsApplication from(Context context) {
        return (CatalogsApplication) MainApplication.from(context);
    }

    public static String getPreferenceLastPath(Context context) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        String def = Environment.getExternalStorageDirectory().getPath();
        String path = shared.getString(CatalogsApplication.PREFERENCE_LAST_PATH, def);
        if (!new File(path).canRead())
            return def;
        return path;
    }

    public static String formatFree(Context context, long free, long d, long u) {
        return context.getString(R.string.free, formatSize(context, free),
                formatSize(context, d) + context.getString(R.string.per_second),
                formatSize(context, u) + context.getString(R.string.per_second));
    }

    public static void setTextNA(View v, String text) {
        TextView t = (TextView) v;
        if (text.isEmpty()) {
            t.setEnabled(false);
            t.setText(R.string.n_a);
        } else {
            t.setEnabled(true);
            t.setText(text);
        }
    }

    public static void setDate(View v, long d) {
        String s = formatDate(d);
        setTextNA(v, s);
    }

    public static String formatDate(long d) {
        if (d == 0)
            return "";
        return SIMPLE.format(new Date(d / 1000000));
    }

    public static int getTheme(Context context, int light, int dark) {
        return MainApplication.getTheme(context, PREFERENCE_THEME, light, dark, context.getString(R.string.Theme_Dark));
    }

    public static String onTrimString(int level) {
        switch (level) {
            case TRIM_MEMORY_COMPLETE:
                return "TRIM_MEMORY_COMPLETE";
            case TRIM_MEMORY_MODERATE:
                return "TRIM_MEMORY_MODERATE";
            case TRIM_MEMORY_BACKGROUND:
                return "TRIM_MEMORY_BACKGROUND";
            case TRIM_MEMORY_UI_HIDDEN:
                return "TRIM_MEMORY_UI_HIDDEN";
            case TRIM_MEMORY_RUNNING_CRITICAL:
                return "TRIM_MEMORY_RUNNING_CRITICAL";
            case TRIM_MEMORY_RUNNING_LOW:
                return "TRIM_MEMORY_RUNNING_LOW";
            case TRIM_MEMORY_RUNNING_MODERATE:
                return "TRIM_MEMORY_RUNNING_MODERATE";
        }
        return "unknown";
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
        zlib = new ZLAndroidApplication() {
            {
                attachBaseContext(CatalogsApplication.this);
                onCreate();
            }
        };
        new HttpClient.SpongyLoader(this, false);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.d(TAG, "onTerminate");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.d(TAG, "onLowMemory");
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.d(TAG, "onTrimMemory: " + onTrimString(level));
    }

    public File getCache() {
        File cache = getExternalCacheDir();
        if (cache == null || !Storage.canWrite(cache))
            cache = getCacheDir();
        return cache;
    }
}
