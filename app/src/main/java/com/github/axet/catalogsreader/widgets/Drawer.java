package com.github.axet.catalogsreader.widgets;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.app.Storage;
import com.github.axet.androidlibrary.preferences.AboutPreferenceCompat;
import com.github.axet.androidlibrary.widgets.ErrorDialog;
import com.github.axet.androidlibrary.widgets.OpenChoicer;
import com.github.axet.androidlibrary.widgets.OpenFileDialog;
import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.github.axet.androidlibrary.widgets.Toast;
import com.github.axet.androidlibrary.widgets.UnreadCountDrawable;
import com.github.axet.catalogsreader.R;
import com.github.axet.catalogsreader.activities.MainActivity;
import com.github.axet.catalogsreader.app.BooksCatalog;
import com.github.axet.catalogsreader.app.BooksCatalogs;
import com.github.axet.catalogsreader.app.EnginesManager;
import com.github.axet.catalogsreader.app.SearchEngine;
import com.github.axet.catalogsreader.app.SearchProxy;
import com.github.axet.catalogsreader.app.CatalogsApplication;
import com.github.axet.catalogsreader.fragments.NavigatorFragment;
import com.github.axet.catalogsreader.navigators.Search;
import com.github.axet.catalogsreader.net.GoogleProxy;
import com.github.axet.catalogsreader.net.TorProxy;
import com.mikepenz.fastadapter.adapters.ItemAdapter;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Drawer implements com.mikepenz.materialdrawer.Drawer.OnDrawerItemClickListener, UnreadCountDrawable.UnreadCount {
    public static final String TAG = Drawer.class.getSimpleName();

    public static final String[] PERMISSIONS = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};

    public static final long ENGINES_AUTO_REFRESH = 12 * AlarmManager.HOUR1; // auto refresh engines every 12 hours

    public static final String LIBRARY = "library";
    public static final String ADD_CATALOG = "add_catalog";
    public static final String SCHEME_CATALOG = "catalog";
    public static final String VIEW_CATALOG = "VIEW_CATALOG";
    public static final int RESULT_ADD_ENGINE = 2;
    public static final int RESULT_ADD_CATALOG = 3;

    Context context;
    MainActivity main;
    Handler handler = new Handler();

    View navigationHeader;
    com.mikepenz.materialdrawer.Drawer drawer;
    UnreadCountDrawable unread;

    Thread update;
    Thread updateOne;
    int updateOneIndex;

    HashMap<ProxyDrawerItem, ProxyDrawerItem.ViewHolder> viewList = new HashMap<>();

    ItemTouchHelper touchHelper;

    long counter = 1;

    public BooksCatalogs catalogs;

    OpenChoicer choicer;

    public static boolean signatureCheck(Context context, int[] dd) {
        for (int d : dd) {
            if (signatureCheck(context, d))
                return true;
        }
        return false;
    }

    public static boolean signatureCheck(Context context, int d) {
        try {
            PackageManager pm = context.getPackageManager();
            Signature[] ss = pm.getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES).signatures;
            for (Signature s : ss) {
                int hash = s.hashCode();
                if (d == hash)
                    return true;
            }
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }

    public Drawer(final MainActivity main, final Toolbar toolbar) {
        this.context = main;
        this.main = main;
        this.catalogs = new BooksCatalogs(main);

        LayoutInflater inflater = LayoutInflater.from(context);

        navigationHeader = inflater.inflate(R.layout.nav_header_main, null, false);

        drawer = new DrawerBuilder()
                .withActivity(main)
                .withToolbar(toolbar)
                .withHeader(navigationHeader)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .withOnDrawerListener(new com.mikepenz.materialdrawer.Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {
                        View view = main.getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                    }

                    @Override
                    public void onDrawerOpened(View drawerView) {
                        long time = System.currentTimeMillis();
                        EnginesManager engies = main.getEngines();
                        long t = engies.getTime();
                        if (t + ENGINES_AUTO_REFRESH < time)
                            refreshEngines(true);
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                    }
                })
                .withOnDrawerItemClickListener(this)
                .build();

        touchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, 0) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                if (!(viewHolder instanceof SearchDrawerItem.ViewHolder))
                    return false;
                if (!(target instanceof SearchDrawerItem.ViewHolder))
                    return false;

                ItemAdapter<IDrawerItem> ad = drawer.getItemAdapter();
                int i = ad.getFastAdapter().getHolderAdapterPosition(viewHolder);
                int k = ad.getFastAdapter().getHolderAdapterPosition(target);
                ad.move(i, k);

                final EnginesManager engines = main.getEngines();
                engines.move(engines.indexOf(ad.getItem(i).getTag()), engines.indexOf(ad.getItem(k).getTag()));

                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            }

            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                if (!(viewHolder instanceof SearchDrawerItem.ViewHolder))
                    return makeMovementFlags(0, 0);
                return super.getMovementFlags(recyclerView, viewHolder);
            }
        });
        touchHelper.attachToRecyclerView(drawer.getRecyclerView());

        Drawable navigationIcon = toolbar.getNavigationIcon();
        unread = new UnreadCountDrawable(context, navigationIcon, Drawer.this);
        unread.setPadding(ThemeUtils.dp2px(main, 15));
        toolbar.setNavigationIcon(unread);

        TextView ver = (TextView) navigationHeader.findViewById(R.id.nav_version);
        AboutPreferenceCompat.setVersion(ver);
    }

    public void close() {
    }

    public void setCheckedItem(long id) {
        drawer.setSelection(id, false);
    }

    public boolean isDrawerOpen() {
        return drawer.isDrawerOpen();
    }

    public void openDrawer() {
        drawer.openDrawer();
    }

    public void closeDrawer() {
        drawer.closeDrawer();
    }

    public void updateUnread() {
        unread.update();
    }

    long getEngineId(Object s) {
        ArrayList<IDrawerItem> list = new ArrayList<>(drawer.getDrawerItems());
        for (int i = 0; i < list.size(); i++) {
            IDrawerItem item = list.get(i);
            Object search = item.getTag();
            if (search == s)
                return item.getIdentifier();
        }
        return -1;
    }

    long getEngineId(List<IDrawerItem> ll, Object s) {
        ArrayList<IDrawerItem> list = new ArrayList<>(drawer.getDrawerItems());
        long id = getEngineId(s);
        if (id != -1)
            return id;
        list.addAll(ll);
        for (int i = 0; i < list.size(); i++) {
            IDrawerItem item = list.get(i);
            if (item.getIdentifier() == counter) {
                counter++;
                if (counter >= 0x00ffffff) // save to set < 0x00ffffff. check View.generateViewId()
                    counter = 1;
                i = -1; // restart search
            }
        }
        return counter;
    }

    public void updateManager() {
        List<IDrawerItem> list = new ArrayList<>();

        KeyguardManager myKM = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        final boolean locked = myKM.inKeyguardRestrictedInputMode();

        for (int i = 0; i < catalogs.getCount(); i++) {
            final BooksCatalog ct = catalogs.getCatalog(i);
            SearchDrawerItem item = new SearchDrawerItem() {
                @Override
                public void bindView(ViewHolder viewHolder) {
                    super.bindView(viewHolder);
                    viewHolder.release.setVisibility(View.GONE);
                    viewHolder.progress.setVisibility(View.INVISIBLE);
                    viewHolder.panel.setVisibility(View.VISIBLE);
                    viewHolder.trash.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle(R.string.book_delete);
                            builder.setMessage(R.string.are_you_sure);
                            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    catalogs.delete(ct.getId());
                                    catalogs.save();
                                    updateManager();
                                }
                            });
                            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            builder.show();
                        }
                    });
                    if (locked) {
                        viewHolder.trash.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        });
                        viewHolder.trash.setColorFilter(Color.GRAY);
                    } else {
                        viewHolder.trash.setColorFilter(ThemeUtils.getThemeColor(context, R.attr.colorAccent));
                    }
                }
            };
            Intent intent = new Intent(LIBRARY);
            intent.putExtra("url", ct.getId());
            item.withIdentifier(getEngineId(list, ct));
            item.withTag(ct);
            item.withName(ct.getTitle());
            item.withIconTintingEnabled(true);
            item.withIcon(R.drawable.ic_drag_handle_black_24dp);
            item.withSelectable(true);
            item.withSetSelected(main.active(ct));
            list.add(item);
        }

        final EnginesManager engines = main.getEngines();

        for (int i = 0; i < engines.getCount(); i++) {
            final Search search = engines.get(i);
            final SearchEngine engine = search.getEngine();

            final int fi = i;
            SearchDrawerItem item = new SearchDrawerItem() {
                @Override
                public void bindView(ViewHolder viewHolder) {
                    super.bindView(viewHolder);

                    viewHolder.release.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (updateOne != null)
                                updateOne.interrupt();
                            updateOneIndex = fi;
                            updateOne = new Thread("Update Engine") {
                                @Override
                                public void run() {
                                    engines.update(fi);
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            updateOne = null;
                                            Search search = engines.get(fi);
                                            main.show(search); // reopen search engine
                                            SearchEngine engine = search.getEngine();
                                            Toast.makeText(context, engine.getName() + context.getString(R.string.engine_updated) + engine.getVersion(), Toast.LENGTH_SHORT).show();
                                            updateManager(); // update proxies list
                                        }
                                    });
                                }
                            };
                            updateOne.start();
                        }
                    });

                    viewHolder.panel.setVisibility(View.GONE);
                    if (updateOne != null && updateOneIndex == fi) {
                        viewHolder.progress.setVisibility(View.VISIBLE);
                        viewHolder.panel.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.progress.setVisibility(View.INVISIBLE);
                    }

                    if (engines.getUpdate(fi)) {
                        viewHolder.release.setVisibility(View.VISIBLE);
                        viewHolder.panel.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.release.setVisibility(View.INVISIBLE);
                    }

                    viewHolder.trash.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle(context.getString(R.string.delete_search));

                            String name = engine.getName();

                            builder.setMessage(name + "\n\n" + context.getString(R.string.are_you_sure));
                            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                    engines.remove(search);
                                    engines.save();
                                    updateManager();
                                }
                            });
                            builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder.show();
                        }
                    });
                    if (locked) {
                        viewHolder.trash.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        });
                        viewHolder.trash.setColorFilter(Color.GRAY);
                    } else {
                        viewHolder.trash.setColorFilter(ThemeUtils.getThemeColor(context, R.attr.colorAccent));
                    }
                }
            };
            item.withIdentifier(getEngineId(list, search));
            item.withTag(search);
            item.withName(engine.getName());
            item.withIconTintingEnabled(true);
            item.withIcon(new UnreadCountDrawable(context, R.drawable.ic_drag_handle_black_24dp, search));
            item.withSelectable(true);
            item.withSetSelected(main.active(search));
            list.add(item);
        }

        updateProxies(list);

        list.add(new SectionDrawerItem()
                .withIdentifier(R.string.menu_settings)
                .withName(R.string.menu_settings));

        AddDrawerItem navadd = new AddDrawerItem() {
            @Override
            public void bindView(ViewHolder viewHolder) {
                super.bindView(viewHolder);
                viewHolder.update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Drawer.this.update != null) {
                            Drawer.this.update.interrupt();
                            Drawer.this.update = null;
                            updateManager();
                        } else {
                            refreshEngines(false);
                        }
                    }
                });
                if (Drawer.this.update != null) {
                    viewHolder.progress.setVisibility(View.VISIBLE);
                    viewHolder.refresh.setVisibility(View.INVISIBLE);
                } else {
                    viewHolder.progress.setVisibility(View.INVISIBLE);
                    viewHolder.refresh.setVisibility(View.VISIBLE);
                }
                if (locked) {
                    viewHolder.update.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    });
                    viewHolder.refresh.setColorFilter(Color.GRAY);
                    viewHolder.update.setEnabled(false);
                } else {
                    viewHolder.refresh.setColorFilter(ThemeUtils.getThemeColor(context, R.attr.colorAccent));
                    viewHolder.update.setEnabled(true);
                }
                withEnabled(!locked);
            }
        };
        navadd.withIdentifier(R.id.nav_add)
                .withName(R.string.add_catalog)
                .withIcon(R.drawable.ic_add_black_24dp)
                .withIconTintingEnabled(true)
                .withSelectable(false);
        list.add(navadd);

        SecondaryDrawerItem desc = new SecondaryDrawerItem() {
            @Override
            public void bindView(ViewHolder viewHolder) {
                super.bindView(viewHolder);
                View name = viewHolder.itemView.findViewById(R.id.material_drawer_name);
                name.setVisibility(View.GONE);
                TextView t = (TextView) viewHolder.itemView.findViewById(R.id.material_drawer_description);
                t.setLines(-1);
                t.setSingleLine(false);
            }
        };
        desc.withDescription(R.string.more_engines);
        desc.withIconTintingEnabled(false);
        desc.withSelectable(false);
        list.add(desc);

        update(list);
    }

    void update(List<IDrawerItem> list) {
        ItemAdapter<IDrawerItem> ad = drawer.getItemAdapter();
        for (int i = ad.getAdapterItemCount() - 1; i >= 0; i--) {
            boolean delete = true;
            for (int k = 0; k < list.size(); k++) {
                if (list.get(k).getIdentifier() == ad.getAdapterItem(i).getIdentifier()) {
                    delete = false;
                    ad.set(ad.getGlobalPosition(i), list.get(k));
                }
            }
            if (delete)
                ad.remove(ad.getGlobalPosition(i));
        }
        for (int k = 0; k < list.size(); k++) {
            boolean add = true;
            for (int i = 0; i < ad.getAdapterItemCount(); i++) {
                if (list.get(k).getIdentifier() == ad.getAdapterItem(i).getIdentifier())
                    add = false;
            }
            if (add)
                ad.add(ad.getGlobalPosition(k), list.get(k));
        }
    }

    public void refreshEngines(final boolean auto) {
        if (update != null) {
            if (auto)
                return;
        }
        final EnginesManager engines = main.getEngines();
        final Thread old = update;
        update = new Thread("Engines Update") {
            @Override
            public void run() {
                if (old != null) {
                    old.interrupt();
                    try {
                        old.join();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        return;
                    }
                }
                boolean a = auto;

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        updateManager();
                    }
                });
                try {
                    engines.refresh();
                } catch (final RuntimeException e) {
                    Log.e(TAG, "Update Engine", e);
                    // only report errors for current active update thread
                    if (update == Thread.currentThread()) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(context, ErrorDialog.toMessage(e), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    a = true; // hide update toast on error
                }
                final boolean b = a;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        update = null;
                        if (!b) {
                            if (!engines.updates())
                                Toast.makeText(context, R.string.no_updates, Toast.LENGTH_SHORT).show();
                        }
                        updateManager();
                    }
                });
            }
        };
        update.start();
    }

    void updateProxies(List<IDrawerItem> list) {
        LayoutInflater inflater = LayoutInflater.from(context);

        View up = inflater.inflate(R.layout.drawer_search_plus, null);
        list.add(new SectionPlusDrawerItem(up)
                .withIdentifier(R.string.web_proxy_s)
                .withName(R.string.web_proxy_s));

        viewList.clear();

        ProxyDrawerItem google = createProxy(GoogleProxy.NAME, R.string.google_proxy);
        list.add(google);

        if (TorProxy.isOrbotInstalled(context)) {
            ProxyDrawerItem tor = createProxy(TorProxy.NAME, R.string.tor_proxy);
            list.add(tor);
        } else { // clear TOR proxy
            final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
            String tag = shared.getString(CatalogsApplication.PREFERENCE_PROXY, "");
            if (tag.equals(TorProxy.NAME)) {
                SharedPreferences.Editor edit = shared.edit();
                edit.putString(CatalogsApplication.PREFERENCE_PROXY, "");
                edit.commit();
            }
        }

        RecyclerView.Adapter a = null;
        Fragment f = main.getCurrentFragment();
        if (f instanceof NavigatorFragment)
            a = (RecyclerView.Adapter) ((NavigatorFragment) f).nav;
        if (a instanceof Search) {
            final SearchEngine engine = ((Search) a).getEngine();
            final SearchProxy p = engine.getProxy();
            if (p != null) {
                final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
                ProxyDrawerItem custom = new ProxyDrawerItem() {
                    @Override
                    public void bindView(final ViewHolder viewHolder) {
                        super.bindView(viewHolder);
                        viewHolder.tag = p.name;
                        viewList.put(this, viewHolder);
                        final Runnable update = new Runnable() {
                            @Override
                            public void run() {
                                String proxy = shared.getString(CatalogsApplication.PREFERENCE_PROXY, "");
                                String proxyp = shared.getString(CatalogsApplication.PREFERENCE_PROXY_PREFIX + engine.getName(), "");
                                if (proxyp.equals(p.name)) {
                                    viewHolder.w.setChecked(true);
                                    for (ViewHolder h : viewList.values()) {
                                        if (h == viewHolder)
                                            continue;
                                        h.w.setChecked(false);
                                    }
                                } else {
                                    for (ProxyDrawerItem i : viewList.keySet()) {
                                        ProxyDrawerItem.ViewHolder h = viewList.get(i);
                                        h.w.setChecked(h.tag.equals(proxy));
                                    }
                                }
                            }
                        };
                        viewHolder.w.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SharedPreferences.Editor edit = shared.edit();
                                if (viewHolder.w.isChecked()) {
                                    edit.putString(CatalogsApplication.PREFERENCE_PROXY_PREFIX + engine.getName(), p.name);
                                } else {
                                    edit.putString(CatalogsApplication.PREFERENCE_PROXY_PREFIX + engine.getName(), "");
                                }
                                edit.commit();
                                update.run();
                            }
                        });
                        update.run();
                    }
                };
                custom.withName(p.name);
                custom.withIcon(R.drawable.ic_vpn_key_black_24dp);
                custom.withIconTintingEnabled(true);
                custom.withSelectable(false);
                list.add(custom);
            }
        }
    }

    ProxyDrawerItem createProxy(final String tag, final int res) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        final String proxy = shared.getString(CatalogsApplication.PREFERENCE_PROXY, "");
        ProxyDrawerItem google = new ProxyDrawerItem() {
            @Override
            public void bindView(final ViewHolder viewHolder) {
                super.bindView(viewHolder);
                viewHolder.tag = tag;
                viewList.put(this, viewHolder);
                viewHolder.w.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SharedPreferences.Editor edit = shared.edit();
                        if (viewHolder.w.isChecked()) {
                            edit.putString(CatalogsApplication.PREFERENCE_PROXY, tag);
                            RecyclerView.Adapter a = null;
                            Fragment f = main.getCurrentFragment();
                            if (f instanceof NavigatorFragment)
                                a = (RecyclerView.Adapter) ((NavigatorFragment) f).nav;
                            if (a instanceof Search) { // remove custom proxy
                                final SearchEngine engine = ((Search) a).getEngine();
                                String proxyp = shared.getString(CatalogsApplication.PREFERENCE_PROXY_PREFIX + engine.getName(), "");
                                if (!proxyp.isEmpty())
                                    edit.putString(CatalogsApplication.PREFERENCE_PROXY_PREFIX + engine.getName(), "");
                            }
                        } else {
                            edit.putString(CatalogsApplication.PREFERENCE_PROXY, "");
                        }
                        edit.commit();
                        for (ViewHolder h : viewList.values()) {
                            if (h == viewHolder)
                                continue;
                            h.w.setChecked(false);
                        }
                    }
                });
                viewHolder.w.setChecked(proxy.equals(tag));
            }
        };
        google.withIdentifier(res);
        google.withName(res);
        google.withIcon(R.drawable.ic_vpn_key_black_24dp);
        google.withIconTintingEnabled(true);
        google.withSelectable(false);
        return google;
    }

    public void openDrawer(Search search) {
        openDrawer();
        EnginesManager engies = main.getEngines();
        for (int i = 0; i < engies.getCount(); i++) {
            if (engies.get(i) == search) {
                setCheckedItem(getEngineId(search));
                main.show(search);
                main.getDrawer().updateManager(); // update proxies
                return;
            }
        }
    }

    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
        long id = drawerItem.getIdentifier();

        final EnginesManager engies = main.getEngines();

        // here only two types of adapters, so setup empty view manually here.

        if (drawerItem.getTag() instanceof Search) {
            Search search = (Search) drawerItem.getTag();
            main.show(search);
            engies.save();
            closeDrawer();
            return true;
        }

        if (drawerItem.getTag() instanceof BooksCatalog) {
            BooksCatalog ct = (BooksCatalog) drawerItem.getTag();
            main.openLibrary(ct);
            engies.save();
            closeDrawer();
            return true;
        }

        if (id == R.id.nav_add) {
            PopupMenu menu = new PopupMenu(context, view);
            main.getMenuInflater().inflate(R.menu.add_catalog, menu.getMenu());
            menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    return onOptionsItemSelected(item);
                }
            });
            menu.show();
            return true;
        }
        return true;
    }

    @Override
    public int getUnreadCount() {
        int count = 0;
        EnginesManager engies = main.getEngines();
        if (engies != null) {
            for (int i = 0; i < engies.getCount(); i++)
                count += engies.get(i).getUnreadCount();
        }
        return count;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_folder) {
            drawer.closeDrawer();
            choicer = new OpenChoicer(OpenFileDialog.DIALOG_TYPE.FOLDER_DIALOG, true) {
                @Override
                public void onResult(Uri uri) {
                    try {
                        BooksCatalog ct = catalogs.loadFolder(uri);
                        catalogs.save();
                        updateManager();
                        main.openLibrary(ct);
                    } catch (Exception e) {
                        ErrorDialog.Post(main, e);
                    }
                }
            };
            choicer.setPermissionsDialog(main, Storage.PERMISSIONS_RO, RESULT_ADD_CATALOG);
            choicer.setStorageAccessFramework(main, RESULT_ADD_CATALOG);
            choicer.show(null);
            return true;
        }

        if (id == R.id.action_json) {
            drawer.closeDrawer();
            choicer = new OpenChoicer(OpenFileDialog.DIALOG_TYPE.FILE_DIALOG, true) {
                @Override
                public void onResult(Uri uri) {
                    try {
                        BooksCatalog ct = catalogs.load(uri);
                        catalogs.save();
                        updateManager();
                        main.openLibrary(ct);
                    } catch (Exception e) {
                        ErrorDialog.Post(main, e);
                    }
                }
            };
            choicer.setPermissionsDialog(main, Storage.PERMISSIONS_RO, RESULT_ADD_CATALOG);
            choicer.setStorageAccessFramework(main, RESULT_ADD_CATALOG);
            choicer.show(null);
            return true;
        }

        return false;
    }

    @TargetApi(21)
    public void onActivityResult(int resultCode, Intent data) {
        choicer.onActivityResult(resultCode, data);
    }

    public void onRequestPermissionsResult(String[] permissions, int[] grantResults) {
        choicer.onRequestPermissionsResult(permissions, grantResults);
    }

    void save(Uri p) {
        final EnginesManager engies = main.getEngines();
        Search search;
        try {
            search = engies.add(p);
        } catch (RuntimeException e) {
            ErrorDialog.Error(main, e);
            return;
        }
        engies.save();
        updateManager();
        openDrawer(search);
    }
}
